
let $c = $('#canvas')
var ctx = $c[0].getContext('2d')

var startX = -1
var startY = -1
var endX = -1
var endY = -1
const ROWS = 10
const COLUMNS = 10
const cellSize = 32

const blackColor = '#000000'
const greenColor = '#00FF00'
const redColor = '#FF0000'
const blueColor = '#0000FF'
const whiteColor = '#FFFFFF'

function drawStrokeRect (color, xCell, yCell) {
  ctx.strokeStyle = color
  ctx.strokeRect(xCell * cellSize, yCell * cellSize, cellSize, cellSize)
}

$c.click(function(event) {
  let xCell = Math.floor(event.offsetX/cellSize)
  let yCell = Math.floor(event.offsetY/cellSize)
  if (xCell >= map.length || yCell >= map[0].length ||
    map[xCell][yCell] === '#') {
    return
  }
  if(startX === -1) {
    startX = xCell
    startY = yCell
    waveAlgo(map, startX, startY)
    drawMap(ctx, map)
    drawStrokeRect(greenColor, startX, startY)
  } else {
    endX = xCell
    endY = yCell
    clearCanvas()
    drawMap(ctx, map)
    drawStrokeRect(greenColor, startX, startY)
    path = minPath(map, endX, endY)
    if (path[path.length-1] !== -1) {
      drawPath(path)
      drawStrokeRect(greenColor, endX, endY)
    } else {
      drawStrokeRect(redColor, endX, endY)
    }

   }
})

function drawMap(context, map) {
  let rows = map.length
  let columns = map[0].length
  ctx.strokeStyle = blackColor
  ctx.strokeRect(0, 0, cellSize * rows, cellSize * columns)
  ctx.fillStyle = blackColor
  for(let i = 0; i < map.length; i++) {
    for(let j = 0; j < map[i].length; j++) {
      let x = i*cellSize
      let y = j*cellSize
      let sign = map[i][j]
      if (sign == '#') {
        ctx.fillRect(x, y, cellSize, cellSize)
        continue;
      }
      ctx.strokeRect(x, y, cellSize, cellSize)
      if (Number.isFinite(sign) && sign != 0) {
        ctx.font = '16px Arial'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillText(sign, x + cellSize/2, y + cellSize/2)
      }
    }
  }
}

function clearCanvas () {
  let rows = map.length
  let columns = map[0].length
  ctx.fillStyle = whiteColor
  ctx.fillRect(0, 0, cellSize * rows, cellSize * columns)
}

function drawPath(path) {
  while (path.length !== 0) {
    let x = path.shift()
    let y = path.shift()
    drawStrokeRect(blueColor, x, y)
  }
}

function init() {
  var map = []
  for(let i = 0; i < ROWS; i++) {
    map[i] = []
    for(let j = 0; j < COLUMNS; j++) {
      map[i].push(' ')
    }
  }
  return map
}

function waveAlgo (map, startX, startY) {
  let queue = []
  queue.push(startX)
  queue.push(startY)
  function isAvailable (x, y) {
    return x >= 0 && x < map.length && y >= 0 && y < map[0].length &&
      !Number.isFinite(map[x][y]) && map[x][y] !== '#'
  }
  map[startX][startY] = 1
  let offset = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0]
  ]
  while (queue.length !== 0) {
    let x = queue.shift()
    let y = queue.shift()
    let turn = map[x][y] + 1
    for (let i = 0; i < offset.length; i += 1) {
      let fixX = x + offset[i][0]
      let fixY = y + offset[i][1]
      if (isAvailable(fixX, fixY)) {
        queue.push(fixX)
        queue.push(fixY)
        map[fixX][fixY] = turn
      }
    }
  }
  return map
}

function minPath(map, endX, endY) {
  let x = endX
  let y = endY
  let path = []
  let offset = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0]
  ]
  function isLessThan (map, x, y, turn) {
    return x >= 0 && x < map.length && y >= 0 && y < map[0].length &&
      Number.isFinite(map[x][y]) && map[x][y] < turn
  }
  let turn = map[x][y]
  while (turn !== 1) {
    var failedSearch = false
    for (let i = 0; i < offset.length; i++) {
      let fixX = x + offset[i][0]
      let fixY = y + offset[i][1]
      if (isLessThan(map, fixX, fixY, turn)) {
        x = fixX
        y = fixY
        path.push(x)
        path.push(y)
        turn = map[x][y]
        break
      }
      if (i === offset.length - 1) {
        failedSearch = true
      }
    }
    if (failedSearch) {
      path.push(-1);
      path.push(-1);
      break;
    }
  }
  return path
}

function generateWalls (map, walls) {
  traps = [
    [['#', '#', '#']],
    [['#'], ['#'], ['#']]
  ]
  for (let i = 0; i < walls;) {
    let randX = Math.floor(Math.random() * map.length)
    let randY = Math.floor(Math.random() * map[0].length)
    let type = 0
    if (Math.floor(Math.random() >= 0.5)) {
       type = 1
    }
    if (traps[type].length <= map.length - (randX + 1) &&
      traps[type][0].length <= map[0].length - (randY + 1)) {
        for (let j = 0; j < traps[type].length; j++) {
          for (let z = 0; z < traps[type][j].length; z++) {
            map[randX + j][randY + z] = traps[type][j][z]
          }
        }
        i++
      } else {
        continue
      }

  }
}

var map = init()
generateWalls(map, 7)
drawMap(ctx, map)
